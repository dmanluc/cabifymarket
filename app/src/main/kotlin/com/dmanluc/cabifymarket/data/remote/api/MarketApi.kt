package com.dmanluc.cabifymarket.data.remote.api

import com.dmanluc.cabifymarket.data.remote.model.MarketApiResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

/**
 * Market API
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    2019-07-02.
 */
interface MarketApi {

    @GET("palcalde/6c19259bd32dd6aafa327fa557859c2f/raw/ba51779474a150ee4367cda4f4ffacdcca479887/Products.json")
    fun getProductsAsync(): Deferred<MarketApiResponse>

}